import { axiosInstancePokeApi } from "../../../../shared/helpers/axiosInstance";
import pokeNetwork from "./pokeNetwork";

const pokemonEndpoints = pokeNetwork(axiosInstancePokeApi);

export const getAllPokemons = pokemonEndpoints.getAllPokemons;
