import { AxiosInstance } from "axios";
import pokemonService from "../services";

const network = (client: AxiosInstance) => ({
  getAllPokemons: async () => client.get(pokemonService().pokemonURL),
});

export default network;
