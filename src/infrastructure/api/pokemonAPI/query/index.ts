import { getAllPokemons } from "../network";
import { useQuery } from "@tanstack/react-query";

const useGetAllPokemons = () => {
  const { data, isLoading, isError, error, isFetching } = useQuery({
    queryKey: ["pokemons"],
    queryFn: getAllPokemons,
    retry: 1,
  });

  const characters = data?.data.results || [];

  return { characters, isLoading, isFetching, isError, error };
};

export default useGetAllPokemons;
