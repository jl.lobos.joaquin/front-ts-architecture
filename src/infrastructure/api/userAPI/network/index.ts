import { axiosInstanceApi } from "../../../../shared/helpers/axiosInstance";
import userNetwork from "./network";

const userEndpoints = userNetwork(axiosInstanceApi);

export const getAllUsers = userEndpoints.getAllUsers;
