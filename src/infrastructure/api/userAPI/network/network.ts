import { AxiosInstance } from "axios";
import userService from "../services";
import { type FindAllUserParams } from "../types/user.type";

const network = (client: AxiosInstance) => ({
  getAllUsers: async (params: FindAllUserParams) =>
    client.get(userService().userURL, { params }),
});

export default network;
