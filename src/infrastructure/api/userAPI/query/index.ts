import { getAllUsers } from "../network";
import { useQuery } from "@tanstack/react-query";
import { type FindAllUserParams } from "../types/user.type";

const useGetAllUsers = (params: FindAllUserParams) => {
  const { data, isLoading, isError, error, isFetching } = useQuery({
    queryKey: ["users", params],
    queryFn: () => getAllUsers(params),
    retry: 1,
  });

  const users = data?.data || [];

  const dataToReturn = {
    users: users?.data || [],
    total: users?.total || 0,
  };

  return {
    users: dataToReturn.users,
    total: dataToReturn.total,
    isLoading,
    isFetching,
    isError,
    error,
  };
};

export default useGetAllUsers;
