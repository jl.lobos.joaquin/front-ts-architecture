export type UserList = {
  id: number;
  name: string;
  email: string;
};

export type UserOrder = {
  name?: string;
  email?: string;
};

export type FindAllUserParams = {
  skip?: number;
  take?: number;
  orderBy?: UserOrder;
};
