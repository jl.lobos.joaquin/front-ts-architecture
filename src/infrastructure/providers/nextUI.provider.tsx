import { NextUIProvider } from "@nextui-org/react";

const ProviderNextUI = ({ children }: { children: React.ReactNode }) => {
  return (
    <NextUIProvider>
      <main className="dark:bg-gray-800 text-foreground bg-background">
        {children}
      </main>
    </NextUIProvider>
  );
};

export default ProviderNextUI;
