import { RouterProvider } from "react-router-dom";
import router from "../../routers/routes";

const ProviderRouterDOM = () => {
  return <RouterProvider router={router} />;
};

export default ProviderRouterDOM;
