import { configureStore } from "@reduxjs/toolkit";
import rootSlices from "./slices";

export const store = configureStore({
  reducer: rootSlices,
  devTools: process.env.NODE_ENV !== "production",
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
