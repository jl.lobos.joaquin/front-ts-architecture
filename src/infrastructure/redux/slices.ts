import { combineReducers } from "redux";
import pokeSlices from "../api/pokemonAPI/slices";

export default combineReducers({
  pokeSlices,
});
