import ReactDOM from "react-dom/client";
import "./ui/styles/index.css";
import ProviderRouterDOM from "./infrastructure/providers/router.provider.tsx";
import ReduxProvider from "./infrastructure/providers/reduxToolkit.provider.tsx";
import ReactQueryProvider from "./infrastructure/providers/reactQuery.provider.tsx";
import ProviderNextUI from "./infrastructure/providers/nextUI.provider.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <ProviderNextUI>
    <ReduxProvider>
      <ReactQueryProvider>
        <ProviderRouterDOM />
      </ReactQueryProvider>
    </ReduxProvider>
  </ProviderNextUI>
);
