import { createBrowserRouter } from "react-router-dom";
import HomeView from "../ui/views/HomeView";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomeView />,
  },
]);

export default router;
