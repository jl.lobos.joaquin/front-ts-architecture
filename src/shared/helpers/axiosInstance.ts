import axios from "axios";

const httpConfigPokeApi = {
  baseURL: "https://pokeapi.co/api/v2/",
};

const axiosInstancePokeApi = axios.create(httpConfigPokeApi);

const httpConfigApi = {
  baseURL: "http://localhost:3000/",
};

const axiosInstanceApi = axios.create(httpConfigApi);

export { axiosInstancePokeApi, axiosInstanceApi };
