import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSun, faMoon } from "@fortawesome/free-solid-svg-icons";

type DarkModeSwitchProps = {
  isDarkMode: boolean;
  toggleDarkMode: () => void;
};

const DarkModeSwitch = ({
  isDarkMode,
  toggleDarkMode,
}: DarkModeSwitchProps) => {
  return (
    <div className="flex justify-end items-center my-2">
      <label className="relative inline-flex items-center justify-between w-16 h-8 bg-gray-300 rounded-full cursor-pointer dark:bg-gray-600">
        <input
          type="checkbox"
          className="sr-only"
          checked={isDarkMode}
          onChange={toggleDarkMode}
        />
        <span
          className={`absolute top-1 bottom-1 left-1 bg-white dark:bg-gray-800 rounded-full w-6 h-6 transform transition-transform ${
            isDarkMode ? "translate-x-8" : ""
          }`}
        ></span>
        <span className="absolute left-2">
          <FontAwesomeIcon
            icon={faSun}
            className={`w-4 h-4 text-yellow-400 ${
              isDarkMode ? "hidden" : "block"
            }`}
          />
        </span>
        <span className="absolute right-2">
          <FontAwesomeIcon
            icon={faMoon}
            className={`w-4 h-4 text-gray-800 dark:text-gray-300 ${
              isDarkMode ? "block" : "hidden"
            }`}
          />
        </span>
      </label>
    </div>
  );
};

export default DarkModeSwitch;
