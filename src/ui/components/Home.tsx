import { IPokemonList } from "../../shared/interface/pokemon";
import { type UserList } from "../../infrastructure/api/userAPI/types/user.type";
import useGetAllPokemons from "../../infrastructure/api/pokemonAPI/query";
import useGetAllUsers from "../../infrastructure/api/userAPI/query";
import { useState } from "react";
import useDarkMode from "../hooks/useDarkMode";
import DarkModeSwitch from "./DarkModeSwitch";

function Home() {
  const [params, setParams] = useState({
    skip: 0,
    take: 1,
    orderBy: {
      name: "asc",
    },
  });

  const { characters } = useGetAllPokemons();
  const { users } = useGetAllUsers(params);

  const handlePageChange = (page: number) => {
    setParams((prev) => ({
      ...prev,
      skip: page * prev.take,
    }));
  };

  const handleSortChange = (sort: string) => {
    setParams((prev) => ({
      ...prev,
      orderBy: {
        name: sort,
      },
    }));
  };

  const { isDarkMode, toggleDarkMode } = useDarkMode();

  return (
    <>
      <div className="text-2xl font-bold text-blue-500 dark:text-red-500">
        Pokemon List
      </div>
      <ul>
        {characters.map((p: IPokemonList) => (
          <li key={p.name}>{p.name}</li>
        ))}
      </ul>
      <br />
      <>
        <DarkModeSwitch
          isDarkMode={isDarkMode}
          toggleDarkMode={toggleDarkMode}
        />
        <div className="text-2xl font-bold">User List</div>
        <ul>
          {users.map((p: UserList) => (
            <li key={p.id}>{p.name}</li>
          ))}
        </ul>
        <div>
          <button onClick={() => handlePageChange(0)}>Página 1</button>
          <button onClick={() => handlePageChange(1)}>Página 2</button>

          <select onChange={(e) => handleSortChange(e.target.value)}>
            <option value="asc">Ascendente</option>
            <option value="desc">Descendente</option>
          </select>
        </div>
      </>
    </>
  );
}

export default Home;
