import { useState, useEffect } from "react";

interface DarkModeHook {
  isDarkMode: boolean;
  toggleDarkMode: () => void;
}

const useDarkMode = (): DarkModeHook => {
  const [isDarkMode, setIsDarkMode] = useState(false);

  useEffect(() => {
    const className = "dark";
    const element = document.documentElement;

    if (isDarkMode) {
      element.classList.add(className);
    } else {
      element.classList.remove(className);
    }
  }, [isDarkMode]);

  const toggleDarkMode = () => setIsDarkMode(!isDarkMode);

  return { isDarkMode, toggleDarkMode };
};

export default useDarkMode;
